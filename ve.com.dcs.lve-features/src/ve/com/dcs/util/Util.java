package ve.com.dcs.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MInvoice;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.globalqss.model.MLCOInvoiceWithholding;

public class Util {

	private static SqlBuilder sqlBuilder = SqlBuilder.builder();

	public static BigDecimal getWithholdingAmt(MInvoice invoice) throws IOException {

		String sql = sqlBuilder.file("sql/getWithholding.sql").build();
		BigDecimal WithholdingAmt = DB.getSQLValueBD(invoice.get_TrxName(), sql, invoice.getC_Invoice_ID());
		return WithholdingAmt;
	}

	public static MLCOInvoiceWithholding[] getWithholdings(MInvoice invoice) {

		String where = "C_Invoice_ID = ? ";

		List<MLCOInvoiceWithholding> withholdings = new Query(invoice.getCtx(), MLCOInvoiceWithholding.Table_Name,
				where, invoice.get_TrxName()).setParameters(invoice.getC_Invoice_ID()).list();

		return withholdings.toArray(new MLCOInvoiceWithholding[withholdings.size()]);

	}
	
	public static MLCOInvoiceWithholding[] getWithholdings(String invoices,Properties ctx,String trxName) {

		String where = "C_Invoice_ID IN ("+invoices+") AND (DocumentNo IS NULL OR DocumentNo LIKE '10%') ";

		List<MLCOInvoiceWithholding> withholdings = new Query(ctx, MLCOInvoiceWithholding.Table_Name,
				where, trxName).setClient_ID().setOrderBy("LCO_WithholdingType_ID").list();

		return withholdings.toArray(new MLCOInvoiceWithholding[withholdings.size()]);

	}
	
	
	
}
