/**********************************************************************
 * This file is part of iDempiere ERP Open Source                      *
 * http://www.idempiere.org                                            *
 *                                                                     *
 * Copyright (C) Contributors                                          *
 *                                                                     *
 * This program is free software; you can redistribute it and/or       *
 * modify it under the terms of the GNU General Public License         *
 * as published by the Free Software Foundation; either version 2      *
 * of the License, or (at your option) any later version.              *
 *                                                                     *
 * This program is distributed in the hope that it will be useful,     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the        *
 * GNU General Public License for more details.                        *
 *                                                                     *
 * You should have received a copy of the GNU General Public License   *
 * along with this program; if not, write to the Free Software         *
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,          *
 * MA 02110-1301, USA.                                                 *
 *                                                                     *
 * Contributors:                                                       *
 * - Carlos Ruiz - globalqss                                           *
 **********************************************************************/

package ve.com.dcs.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.globalqss.model.LCO_MInvoice;
import org.globalqss.model.X_LCO_InvoiceWithholding;

import ve.com.dcs.base.CustomProcess;

/**
 *	LCO_GenerateWithholding
 *
 *  @author Carlos Ruiz - globalqss - Quality Systems & Solutions - http://globalqss.com
 */
public class LCO_GenerateWithholdingAfter extends CustomProcess
{

	/** The Record						*/
	//private int		p_Record_ID = 0;
	private String p_C_Invoice_ID ;
	private Timestamp p_DateTrx = null;
	private Timestamp p_DateAcct = null;
	private String p_WithholdingNo ;
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_Invoice_ID")) {
				p_C_Invoice_ID = para[i].getParameterAsString();

			}else if (name.equals("DateTrx")) {
				p_DateTrx = para[i].getParameterAsTimestamp();

			}else if (name.equals("WithholdingNo")) {
				p_WithholdingNo = para[i].getParameterAsString();
	
			}else if (name.equals("DateAcct")) {
				p_DateAcct = para[i].getParameterAsTimestamp();

			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		//p_Record_ID = p_C_Invoice_ID;
	}	//	prepare

	/**
	 * 	Process
	 *	@return message
	 *	@throws Exception
	 */
	protected String doIt() throws Exception
	{
		List<String> listinvoices = new ArrayList<>(Arrays.asList(p_C_Invoice_ID.split(",")));
		int cnt = 0;
		if (! MSysConfig.getBooleanValue("LCO_USE_WITHHOLDINGS", true, Env.getAD_Client_ID(Env.getCtx())))
			return "@invalid@";
		
		for(String invoice : listinvoices) {
			LCO_MInvoice inv = new LCO_MInvoice(getCtx(), Integer.valueOf(invoice), get_TrxName());
			if (inv.getC_Invoice_ID() == 0)
				throw new AdempiereUserError("@No@ @Invoice@");

			cnt = inv.recalcWithholdings();

			if (cnt == -1)
				throw new AdempiereUserError("Error calculating withholding, please check log");




			DB.executeUpdateEx("Update LCO_InvoiceWithholding set DateAcct=?, DateTrx=?, DocumentNo=? where C_Invoice_ID=? and  LCO_WithholdingType_ID NOT IN (SELECT LCO_WithholdingType_ID from LCO_WithholdingType WHERE type='Other')",
					new Object[] {p_DateAcct,p_DateTrx, p_WithholdingNo,Integer.valueOf(invoice)},get_TrxName());


		}
		return "@Inserted@=" + cnt;
	}	//	doIt

}	//	LCO_GenerateWithholding
