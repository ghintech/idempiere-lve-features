package ve.com.dcs.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MInvoice;
import org.compiere.model.MOrg;
import org.compiere.model.MSequence;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;
import org.globalqss.model.MLCOInvoiceWithholding;
import org.globalqss.model.X_LCO_WithholdingType;

import ve.com.dcs.base.CustomProcess;
import ve.com.dcs.model.MLVEWithholdingSequence;

public class LVE_GenerateWithholdingSequence extends CustomProcess{


	private String p_C_Invoice_ID ;

	private Timestamp p_DateAcct = null;

	private int p_C_BPartner_ID=0;
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_BPartner_ID")) {
				p_C_BPartner_ID = para[i].getParameterAsInt();

			}else if (name.equals("C_Invoice_ID")) {
				p_C_Invoice_ID = para[i].getParameterAsString();

			}else if (name.equals("DateAcct")) {
				p_DateAcct = para[i].getParameterAsTimestamp();

			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		//p_Record_ID = p_C_Invoice_ID;
	}	//	prepare

	@Override
	protected String doIt() throws Exception {

		if (! MSysConfig.getBooleanValue("LCO_USE_WITHHOLDINGS", true, Env.getAD_Client_ID(Env.getCtx())))
			return "@invalid@";

		String where ="DateAcct=? and C_Invoice_ID IN (SELECT C_Invoice_ID FROM LCO_InvoiceWithholding) AND DocStatus IN ('CO','CL')";
		if(p_C_Invoice_ID!=null)
			where = where+" AND C_Invoice_ID IN ("+p_C_Invoice_ID+")";
		List<MInvoice> listinvoices = new Query(getCtx(), MInvoice.Table_Name, 
				where, get_TrxName())
				.setClient_ID().setParameters(p_DateAcct).setOrderBy("C_BPartner_ID").list();
		//int cnt = 0;
		int C_BPartner_ID=0;
		String stringinvoices="";
		for(MInvoice auxinvoice : listinvoices) {
			
			
			if(C_BPartner_ID==0 || C_BPartner_ID==auxinvoice.getC_BPartner_ID()) {
				
				if(stringinvoices.compareTo("")==0)
					stringinvoices=String.valueOf(auxinvoice.get_ID());
				else
					stringinvoices=stringinvoices+","+String.valueOf(auxinvoice.get_ID());

			}else {
				// Create Withholding Sequence
				MLCOInvoiceWithholding[] withholdings = ve.com.dcs.util.Util.getWithholdings(stringinvoices,getCtx(),get_TrxName());
				int wt=0;
				MSequence lastseq=null;
				String DocumentNo="";
				for (MLCOInvoiceWithholding wh : withholdings) {

					MInvoice invoice = new MInvoice(getCtx(), wh.getC_Invoice_ID(),get_TrxName() );
					X_LCO_WithholdingType wht = new X_LCO_WithholdingType(getCtx(), wh.getLCO_WithholdingType_ID(),
							get_TrxName());

					if (wht.get_ValueAsBoolean("IsControlledBySequence")) {

						MLVEWithholdingSequence whseq = MLVEWithholdingSequence.getSequence(wh);
						if (whseq==null)
							throw new AdempiereUserError("No existe una secuencia creada para la organizacion "+MOrg.get(invoice.getAD_Client_ID()).getName());



						//MSequence seq = new MSequence(invoice.getCtx(), whseq.getAD_Sequence_ID(), invoice.get_TrxName());
						//DocumentNo=DocumentNo + MSequence.getDocumentNoFromSeq(seq, invoice.get_TrxName(), seq);
						//wh.setDocumentNo(DocumentNo);

						if( wt!=wh.getLCO_WithholdingType_ID() || wt==0) {
							String TempDocumentNo = invoice.getDateAcct().toString();
							DocumentNo = TempDocumentNo.substring(0, 4);
							DocumentNo = DocumentNo.concat(TempDocumentNo.substring(5, 7));
							//lastseq.getCurrentNext();
							//lastseq.saveEx();
							lastseq=new MSequence(invoice.getCtx(), whseq.getAD_Sequence_ID(), invoice.get_TrxName());
							DocumentNo=DocumentNo + MSequence.getDocumentNoFromSeq(lastseq, invoice.get_TrxName(), lastseq);
						}
						wt=wh.getLCO_WithholdingType_ID();
						wh.setDocumentNo(DocumentNo);
						wh.saveEx();

					}
					stringinvoices=String.valueOf(auxinvoice.get_ID());
				}
				
				
			}
			C_BPartner_ID=auxinvoice.getC_BPartner_ID();
			
			
		}
		if(stringinvoices.compareTo("")!=0){
			// Create Withholding Sequence
			MLCOInvoiceWithholding[] withholdings = ve.com.dcs.util.Util.getWithholdings(stringinvoices,getCtx(),get_TrxName());
			int wt=0;
			MSequence lastseq=null;
			String DocumentNo="";
			for (MLCOInvoiceWithholding wh : withholdings) {

				MInvoice invoice = new MInvoice(getCtx(), wh.getC_Invoice_ID(),get_TrxName() );
				X_LCO_WithholdingType wht = new X_LCO_WithholdingType(getCtx(), wh.getLCO_WithholdingType_ID(),
						get_TrxName());

				if (wht.get_ValueAsBoolean("IsControlledBySequence")) {

					MLVEWithholdingSequence whseq = MLVEWithholdingSequence.getSequence(wh);
					if (whseq==null)
						throw new AdempiereUserError("No existe una secuencia creada para la organizacion "+MOrg.get(invoice.getAD_Client_ID()).getName());



					//MSequence seq = new MSequence(invoice.getCtx(), whseq.getAD_Sequence_ID(), invoice.get_TrxName());
					//DocumentNo=DocumentNo + MSequence.getDocumentNoFromSeq(seq, invoice.get_TrxName(), seq);
					//wh.setDocumentNo(DocumentNo);

					if( wt!=wh.getLCO_WithholdingType_ID() || wt==0) {
						String TempDocumentNo = invoice.getDateAcct().toString();
						DocumentNo = TempDocumentNo.substring(0, 4);
						DocumentNo = DocumentNo.concat(TempDocumentNo.substring(5, 7));
						//lastseq.getCurrentNext();
						//lastseq.saveEx();
						lastseq=new MSequence(invoice.getCtx(), whseq.getAD_Sequence_ID(), invoice.get_TrxName());
						DocumentNo=DocumentNo + MSequence.getDocumentNoFromSeq(lastseq, invoice.get_TrxName(), lastseq);
					}
					wt=wh.getLCO_WithholdingType_ID();
					wh.setDocumentNo(DocumentNo);
					wh.saveEx();

				}
				
			}
			
			
		}
		return "ok";
	}

}
