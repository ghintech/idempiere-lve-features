@echo off

set DEBUG_MODE=

if "%1" == "debug" (
  set DEBUG_MODE=debug
)

cd ve.com.dcs.lve-features.targetplatform
call .\plugin-builder.bat %DEBUG_MODE% ..\ve.com.dcs.lve-features ..\ve.com.dcs.lve-features.test
cd ..
